//
//  AppDelegate.h
//  DoodleJump
//
//  Created by Chirag's Mac on 2/19/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

