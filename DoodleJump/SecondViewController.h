//
//  SecondViewController.h
//  DoodleJump
//
//  Created by Chirag's Mac on 2/21/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameView.h"
#import "SegueDelegate.h"

int jumpvalue=0;
NSTimer *falling;


@interface SecondViewController : UIViewController
{
    id<SegueDelegate> segueDelegate;

    
}
@property (nonatomic, strong) IBOutlet GameView *gameView;
@property (strong, nonatomic) IBOutlet UIImageView *Gameover;

@property (strong, nonatomic) IBOutlet UIButton *Restart;
@property (strong, nonatomic) IBOutlet UILabel *score2;
@property (strong, nonatomic) IBOutlet UILabel *score1;

@end
