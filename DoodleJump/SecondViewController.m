//
//  SecondViewController.m
//  DoodleJump
//
//  Created by Chirag's Mac on 2/21/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import "SecondViewController.h"



@interface SecondViewController ()
@property (nonatomic, strong) CADisplayLink *displayLink;
@end

@implementation SecondViewController
@synthesize gameView,Gameover,Restart,score1,score2;




//@synthesize jumper,platform,platform2,platform3,platform4,tilt;

/*

-(void)fallingcode
{
    jumpvalue=jumpvalue - 5;
    jumper.center = CGPointMake(jumper.center.x, jumper.center.y - jumpvalue);
     NSLog(@"Jump %i", jumpvalue);
    CGPoint a = jumper.center;
    CGPoint b = platform.center;
    CGFloat distance = sqrt(pow((b.x - a.x),2) + pow((b.y - a.y),2));
    
    if(distance < (jumper.bounds.size.width/2.0 + platform.bounds.size.width/2.0)){
        jumpvalue=45;
    }
 
   
    jumpvalue=jumpvalue - 5;
    jumper.center = CGPointMake(jumper.center.x, jumper.center.y - jumpvalue);
     NSLog(@"Jump %i", jumpvalue);
    jumper.center = CGPointMake(jumper.center.x + tilt , jumper.center.y);
    if(CGRectIntersectsRect(jumper.frame, platform.frame))
    {
       // jumper.center = CGPointMake(jumper.center.x, platform.center.y -37);
        jumpvalue=40;
        
    }
    if(CGRectIntersectsRect(jumper.frame, platform2.frame))
    {
        // jumper.center = CGPointMake(jumper.center.x, platform.center.y -37);
        jumpvalue=40;
        
    }
    if(CGRectIntersectsRect(jumper.frame, platform3.frame))
    {
        // jumper.center = CGPointMake(jumper.center.x, platform.center.y -37);
        jumpvalue=40;
        
    }
    if(CGRectIntersectsRect(jumper.frame, platform4.frame))
    {
        // jumper.center = CGPointMake(jumper.center.x, platform.center.y -37);
        jumpvalue=40;
        
    }

}
*/
-(IBAction)speedChange:(id)sender
{
    UISlider *s = (UISlider *)sender;
   //NSLog(@"tilt %f", (float)[s value]);
    [gameView setTilt:(float)[s value]];
   // tilt = (float)[s value];
    
}




- (void)viewDidLoad {
    [super viewDidLoad];
    Gameover.hidden=true;
    Restart.hidden=true;
    score1.hidden=true;
    score2.hidden=true;
   
    _displayLink = [CADisplayLink displayLinkWithTarget:gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:100];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
    [gameView generateNewBricks:ON_SCREEN_BRICKS];
    [gameView generateNewBricks:ABOVE_SCREEN_BRICKS];
    
    [gameView setSegueDelegate:self];

}



-(void) gameOverSegue{
 //   [self performSegueWithIdentifier:@";" sender:self];
    //NSLog(@"Here");
    Gameover.hidden=false;
    score1.hidden=false;
    score2.hidden=false;
    [score2 setText:[NSString stringWithFormat:@"%d",rand() % 1000]];
    [self.view bringSubviewToFront:Gameover];
    Restart.hidden=false;
    [self.view bringSubviewToFront:Restart];
    [self.view bringSubviewToFront:score1];
    [self.view bringSubviewToFront:score2];
    _displayLink.paused = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
