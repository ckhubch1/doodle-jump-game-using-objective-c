//
//  Jumper.h
//  DoodleJump
//
//  Created by Chirag's Mac on 2/24/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Jumper : UIImageView

@property (nonatomic) double dx, dy;  //Velocity of jumper
@end
