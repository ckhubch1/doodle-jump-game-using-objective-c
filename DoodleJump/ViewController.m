//
//  ViewController.m
//  2048-Game
//
//  Created by Chiragkishore Khubchandani on 2/15/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize label1,label01,label02,label03,label05,Image1;
NSTimer *timer;
BOOL blinkStatus = NO;



- (void)viewDidLoad {
    

    
    [label01 setText: @""];
    [label02 setText: @""];
    [label03 setText: @""];
    [label05 setText: @""];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [NSTimer
                      scheduledTimerWithTimeInterval:(NSTimeInterval)(0.4)
                      target:self
                      selector:@selector(blink)
                      userInfo:nil
                      repeats:TRUE];
    
    CGPoint origin = self.Image1.center;
    CGPoint target = CGPointMake(self.Image1.center.x, self.Image1.center.y+90);
    CABasicAnimation *bounce = [CABasicAnimation animationWithKeyPath:@"position.y"];
    bounce.duration = 0.6;
    bounce.fromValue = [NSNumber numberWithInt:origin.y];
    bounce.toValue = [NSNumber numberWithInt:target.y];
    bounce.repeatCount = 100;
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"jump" ofType:@"wav"];
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &sound);
     AudioServicesPlaySystemSound(sound);
     
    bounce.autoreverses = YES;
    [self.Image1.layer addAnimation:bounce forKey:@"position"];
}

-(void)blink{
    if(blinkStatus == NO){
        [label1 setText:@""];
        [label1 setText:@"Doodle Jump"];
        blinkStatus = YES;
    }else {
        
        blinkStatus = NO;
    }
}

- (IBAction)instructions:(id)sender {
    
    [label01 setText:@"Click play to Begin."];
    [label02 setText:@"Use screen swipe to move across bricks."];
    [label03 setText:@"If you touch the ground you lose."];
    [label05 setText:@"Have fun!!"];

}

- (IBAction)about:(id)sender {
    [label01 setText:@"Game is suitable for swipe gestures."];
    [label02 setText:@"Has in built music."];
    [label03 setText:@"Has Score calculation and Restart option."];
    [label05 setText:@"By Chirag Khubchandani"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
