//
//  GameView.h
//  DoodleJump
//
//  Created by Chirag's Mac on 2/24/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "SegueDelegate.h"
#import <AudioToolbox/AudioToolbox.h>

#define BRICKS_1 2
#define BRICKS_2 1
#define BRICKS_3 1
#define BRICKS_4 1
#define BRICKS_5 2
#define BRICKS_6 1
#define BRICKS_7 2
#define BRICKS_8 1
#define BRICKS_9 1

typedef enum {
    ON_SCREEN_BRICKS = 0,
    ABOVE_SCREEN_BRICKS = 1
}BrickGeneratorMode;

@interface GameView : UIView {
    int scorev;
    IBOutlet UILabel *score;
    id<SegueDelegate> segueDelegate;
    int bricksDown;
    int bricksInRegion1;
    int bricksInRegion2;
    int bricksInRegion3;
    int bricksInRegion4;
    int bricksInRegion5;
    int bricksInRegion6;
    int bricksInRegion7;
    int bricksInRegion8;
    int bricksInRegion9;
    bool gameOver;
    SystemSoundID sound;
    
}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic, strong) NSMutableArray *wallpaper;
@property (nonatomic) float tilt;

-(void) generateNewBricks:(BrickGeneratorMode) mode;
-(bool) checkOverlap:(UIImageView *) newBrick;
-(void) moveBricksDown:(int)distanceToMove;
-(void) setSegueDelegate:(id<SegueDelegate>)delegate;

-(void)arrange:(CADisplayLink *)sender;

@end


