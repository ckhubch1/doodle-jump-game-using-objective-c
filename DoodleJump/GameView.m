//
//  GameView.m
//  DoodleJump
//
//  Created by Chirag's Mac on 2/24/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#import "GameView.h"


@implementation GameView

@synthesize jumper,bricks,tilt,wallpaper;

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if(self){
        CGRect bounds = [self bounds];
        [score setText: @""];
        UIImage *jumperImage = [UIImage imageNamed:@"Doodler.png"];
        jumper = [[Jumper alloc] initWithImage:jumperImage];
        [jumper setFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 120, 50, 60)];
        [jumper setDx:0];
        [jumper setDy:10];
         bricksDown = 0;
        gameOver = NO;
        bricks = [[NSMutableArray alloc] init];
        
        //Create brick where jumper spawns
        UIImage *brickImage = [UIImage imageNamed:@"bricks.jpg"];
        UIImageView *brick = [[UIImageView alloc] initWithImage:brickImage];
        float height = 12;
        float width = bounds.size.width * .2;
        [brick setFrame:CGRectMake(bounds.size.width/2 - 25, bounds.size.height - 120, width, height)];
        
        [bricks addObject:brick];
        // NSLog(@"Bounce11!");
        [self addSubview:jumper];
        [self addSubview:brick];
        //[self makeBricks:nil];
  
        bricksDown = 0;
        
        bricksInRegion1 = 0;
        bricksInRegion2 = 0;
        bricksInRegion3 = 0;
        bricksInRegion4 = 0;
        bricksInRegion5 = 0;
    }
    return self;
}

/*-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    
    if (bricks)
    {
        for (UIImageView *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; ++i)
    {
        UIImage *brickImage = [UIImage imageNamed:@"bricks.jpg"];
         UIImageView *brick = [[UIImageView alloc] initWithImage:brickImage];
        float height = 20;
        float width = bounds.size.width * .3;
        
        int xCoord = arc4random() % (int)bounds.size.width * .8;
        int yCoord = arc4random() % (int)bounds.size.height *.8;

       [brick setFrame:CGRectMake(xCoord, yCoord, width, height)];
        //[brick setFrame:CGRectMake(bounds.size.width/2 - 25, bounds.size.height - 100, width, height)];
        [self addSubview:brick];
     //   [brick setCenter:CGPointMake(rand() % (int)(bounds.size.width * .9), rand() % (int)(bounds.size.height * .9))];
        [bricks addObject:brick];
    }
}
*/


-(void)arrange:(CADisplayLink *)sender
{

    CGPoint p = [jumper center];
    CGRect jumperBounds = [jumper bounds];
    CGRect bounds = [self bounds];
    double midHeight = bounds.size.height / 2;
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 4)
        [jumper setDx:4];
    if ([jumper dx] < -4)
        [jumper setDx:-4];

    
    
    // Move jumper in direction of their y velocity

    p.x += [jumper dx];
    p.y -= [jumper dy];      // positive y velocity will move up, negative down
    
    double jumperBottom = p.y + jumperBounds.size.height / 2;
    CGPoint bottomOfJumper = p;
    bottomOfJumper.y = jumperBottom;
    
    if(jumperBottom >= bounds.size.height)
    {
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"d5" ofType:@"wav"];
        NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &sound);
        AudioServicesPlaySystemSound(sound);
        
        
        gameOver = YES;
        [segueDelegate gameOverSegue];
        
    }

       
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If moving down and touch a brick boost velocity to bounce up
    if([jumper dy] < 0){
        for (UIImageView *brick in bricks){
            CGRect brickFrame = [brick frame];
            if(CGRectContainsPoint(brickFrame, bottomOfJumper)){
                NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"jump" ofType:@"wav"];
                NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
                AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &sound);
                AudioServicesPlaySystemSound(sound);
                int val= rand() % 70;
                scorev=scorev+val;
                [score setText:[NSString stringWithFormat:@"%d",scorev]];
                [jumper setDy:10];
                
    
    
    
    
            }
        }
    }
 //   [jumper setCenter:p];
    

    jumperBottom = p.y + jumperBounds.size.height / 2;
    
    // If jumper halfway above screen
    if(jumperBottom <= midHeight){
        p.y += midHeight - jumperBottom;
        [self moveBricksDown:midHeight - jumperBottom];
    }
    // Moving bricks down by 1 pixel
    if(bricksDown >= bounds.size.height){
        bricksDown = 0;
        [self generateNewBricks:ABOVE_SCREEN_BRICKS];
    }
    
    // Removing bricks going below the screen
    [self removeOldBricks];
    
    [jumper setCenter:p];


}



-(void) generateNewBricks:(BrickGeneratorMode) mode{
    CGRect bounds = [self bounds];
    float height = 12;
    
    // spawning 9 bricks all apart
   // int i = 0;
    
    
    int NUM_REGIONS = 9;
    bricksInRegion1 = 0;
    bricksInRegion2 = 0;
    bricksInRegion3 = 0;
    bricksInRegion4 = 0;
    bricksInRegion5 = 0;
    bricksInRegion6 = 0;
    bricksInRegion7 = 0;
    bricksInRegion8 = 0;
    bricksInRegion9 = 0;
   
    
    
  
    while(![self allBrickRegionsFull]){
        float width = bounds.size.width * .2;
    //Removing overlapping bricks
        UIImageView *brick;
        bool brickRegionFull;
        do{
            brickRegionFull = NO;
            UIImage *brickImage = [UIImage imageNamed:@"bricks.jpg"];
            brick = [[UIImageView alloc] initWithImage:brickImage];
            
            int xCoord = arc4random() % (int)bounds.size.width * .8;
            int yCoord = arc4random() % (int)bounds.size.height;
            
            if(yCoord >= 0 && yCoord < bounds.size.height / NUM_REGIONS)
                brickRegionFull = [self brickRegionFull:1 numBricks:bricksInRegion1 + 1];
            
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS && yCoord < bounds.size.height / NUM_REGIONS * 2)
                brickRegionFull = [self brickRegionFull:2 numBricks:bricksInRegion2 + 1];
           
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS * 2 && yCoord < bounds.size.height / NUM_REGIONS * 3)
                brickRegionFull = [self brickRegionFull:3 numBricks:bricksInRegion3 + 1];
            
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS * 3 && yCoord < bounds.size.height / NUM_REGIONS * 4)
                brickRegionFull = [self brickRegionFull:4 numBricks:bricksInRegion4 + 1];
           
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS * 4 && yCoord < bounds.size.height / NUM_REGIONS * 5)
                brickRegionFull = [self brickRegionFull:5 numBricks:bricksInRegion5 + 1];
          
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS * 5 && yCoord < bounds.size.height / NUM_REGIONS * 6)
                brickRegionFull = [self brickRegionFull:6 numBricks:bricksInRegion6 + 1];
            
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS * 6 && yCoord < bounds.size.height / NUM_REGIONS * 7)
                brickRegionFull = [self brickRegionFull:7 numBricks:bricksInRegion7 + 1];
          
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS * 7 && yCoord < bounds.size.height / NUM_REGIONS * 8)
                brickRegionFull = [self brickRegionFull:8 numBricks:bricksInRegion8 + 1];
         
            
            else if(yCoord >= bounds.size.height / NUM_REGIONS * 8 && yCoord < bounds.size.height)
                brickRegionFull = [self brickRegionFull:9 numBricks:bricksInRegion9 + 1];
            
            if(mode == ABOVE_SCREEN_BRICKS)
                yCoord *= -1;
            
            [brick setFrame:CGRectMake(xCoord, yCoord, width, height)];
        } while([self checkOverlap:brick] || brickRegionFull);
        
        
        
        
        float finalYCoord = [brick frame].origin.y;
        
        //Generate bricks on new screen
        if(mode == ABOVE_SCREEN_BRICKS)
            finalYCoord *= -1;
        
        if(finalYCoord >= 0 && finalYCoord < bounds.size.height / NUM_REGIONS)
            bricksInRegion1++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS && finalYCoord < bounds.size.height / NUM_REGIONS * 2)
            bricksInRegion2++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS * 2 && finalYCoord < bounds.size.height / NUM_REGIONS * 3)
            bricksInRegion3++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS * 3 && finalYCoord < bounds.size.height / NUM_REGIONS * 4)
            bricksInRegion4++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS * 4 && finalYCoord < bounds.size.height / NUM_REGIONS * 5)
            bricksInRegion5++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS * 5 && finalYCoord < bounds.size.height / NUM_REGIONS * 6)
            bricksInRegion6++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS * 6 && finalYCoord < bounds.size.height / NUM_REGIONS * 7)
            bricksInRegion7++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS * 7 && finalYCoord < bounds.size.height / NUM_REGIONS * 8)
            bricksInRegion8++;
        else if(finalYCoord >= bounds.size.height / NUM_REGIONS * 8 && finalYCoord < bounds.size.height)
            bricksInRegion9++;
        
        
        [bricks addObject:brick];
        [self addSubview:brick];
    }
}

-(void) setSegueDelegate:(id<SegueDelegate>)delegate{
    segueDelegate = delegate;
}


//Check if the region is full of bricks
-(bool) brickRegionFull:(int) region numBricks:(int) numBricks{
    
    bool regionFull = NO;
    switch (region) {
        case 1:
            if (numBricks > BRICKS_1)
                regionFull = YES;
            break;
        case 2:
            if (numBricks > BRICKS_2)
                regionFull = YES;
            break;
        case 3:
            if (numBricks > BRICKS_3)
                regionFull = YES;
            break;
        case 4:
            if (numBricks > BRICKS_4)
                regionFull = YES;
            break;
        case 5:
            if (numBricks > BRICKS_5)
                regionFull = YES;
            break;
        case 6:
            if (numBricks > BRICKS_6)
                regionFull = YES;
            break;
        case 7:
            if (numBricks > BRICKS_7)
                regionFull = YES;
            break;
        case 8:
            if (numBricks > BRICKS_8)
                regionFull = YES;
            break;
        case 9:
            if (numBricks > BRICKS_9)
                regionFull = YES;
            break;
    }
    
    return regionFull;
}


-(bool) allBrickRegionsFull{
    return bricksInRegion1 == BRICKS_1 && bricksInRegion2 == BRICKS_2 &&
    bricksInRegion3 == BRICKS_3 && bricksInRegion4 == BRICKS_4 &&
    bricksInRegion5 == BRICKS_5 && bricksInRegion6 == BRICKS_6 &&
    bricksInRegion7 == BRICKS_7 && bricksInRegion8 == BRICKS_8 &&
    bricksInRegion9 == BRICKS_9;
}


-(bool) checkOverlap:(UIImageView *) newBrick{
    int widthExtension = 50;
    int heightExtension = 30;
    for(UIImageView *existingBrick in bricks){
        CGRect extendedFrame = [existingBrick frame];
        extendedFrame.size.width += widthExtension;
        extendedFrame.size.height += heightExtension;
        if(CGRectIntersectsRect([newBrick frame], extendedFrame))
            return YES;
    }
    return NO;
}



-(void) moveBricksDown:(int) distanceToMove{
    
    bricksDown += distanceToMove;
    for(UIImageView *brick in bricks){
        CGPoint newPos = [brick center];
        newPos.y += distanceToMove;
        [brick setCenter:newPos];
    }
}


// brick has fallen below visible part of screen - delete it
-(void) removeOldBricks{

    CGRect screenBounds = [self bounds];
        for(int i = 0; i < [bricks count]; i++){
        UIImageView *brick = bricks[i];
        CGPoint brickOrigin = [brick frame].origin;
        
        if(brickOrigin.y> screenBounds.size.height )
        {
           [bricks removeObject:brick];
           // NSLog(@"Down");
        }
    }
    
   }



/*-(void) pushBricksDown:(int) distanceToPush{
    
   bricksgodown += distanceToPush;
 
    for(UIImageView *brick in bricks){
        CGPoint newPos = [brick center];
        newPos.y += distanceToPush;
        [brick setCenter:newPos];
    }
}
*/

@end
