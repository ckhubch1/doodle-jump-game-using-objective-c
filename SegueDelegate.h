//
//  SegueDelegate.h
//  DoodleJump
//
//  Created by Chirag's Mac on 2/26/17.
//  Copyright © 2017 Chirag Khubchandani. All rights reserved.
//

#ifndef SegueDelegate_h
#define SegueDelegate_h

@protocol SegueDelegate <NSObject>

-(void) gameOverSegue;

@end

#endif /* SegueDelegate_h */
